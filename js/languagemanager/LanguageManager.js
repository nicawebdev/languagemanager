/**
 * @version v1.0.0.0
 * 
 * 
 * @author nica nicolacastellaniwebdev@gmail.com
 * @description This plugin manage all the data-dkey elements around the website
 * 
 */


/**
 * @description init function for plugin
 * @param {HTML Object} picker the main language picker, used to change website lang
 */

var LanguageManager = function(picker, settings) {
    return this.init(picker, settings);
}

LanguageManager.prototype = {
    picker: null, // picker html object
    instance: null, // current plugin instance
    hasPicker: true, // if website has a select language picker than manage change event
    currentLang: null, // current browser lang
    settings: {},
    elementsToDictionarize: null,
    currentScreenWidthRatio: 'large',
    isResponsive: false,
    init: function(_picker,_settings){

        if(!this.instance) {
            instance = this;
        }

        instance.picker = _picker;
        instance.loadSettings(function(){
            if(_settings)
                Object.assign(instance.settings.settings, _settings);

            instance.currentLang = instance.settings.settings.defaultLang || 'browser';
            
            if(instance.currentLang == 'browser') {
                instance.currentLang = navigator.language || navigator.userLanguage;
                instance.currentLang = instance.currentLang.substring(0,2);
            }

            if(!instance.settings.settings.forcedLang) {
                var lm__clang = window.sessionStorage.getItem('lm__clang');

                if(instance.settings.settings.cacheLanguage && lm__clang && instance.settings.settings.langs.indexOf(lm__clang) > -1) {
                    instance.currentLang = lm__clang;
                    console.warn("[LanguageManager] Cached language detected: " + instance.settings.settings.currentLang + ".");
                }
            } else {
                instance.currentLang = instance.settings.settings.forcedLang;
                console.warn('[LanguageManager] Forced language detected: ' + instance.currentLang + ".")
            }

            if(instance.settings.settings.cacheLanguage && !lm__clang){
                window.sessionStorage.setItem('lm__clang', instance.currentLang);
            }

            if(instance.settings.settings.manageResponsive) {
                instance.isResponsive = true;
                instance.prepareForResponsive();
            } else {
                instance.prepareLabels();
            }

            instance.initPicker();
        });

        return instance;
    },

    loadSettings: function(cb) {

        instance.loadJSON('js/languagemanager/config/lmconfig.json', function(d){
            instance.settings = JSON.parse(d);
            instance.elementsToDictionarize = document.querySelectorAll('[data-dkey]');
            (cb||$.noop)();
        });

    },

    initPicker: function() {
        console.warn("[LanguageManager] Current language detected: " + instance.currentLang + ".");
        if(!instance.picker) {
            instance.hasPicker = false;
            console.warn("[LanguageManager] No picker selected, LanguageManager will detect language according with browser language.");
            return;
        }

        var enabledLangs = instance.settings.settings.langs;

        enabledLangs = enabledLangs.split('|');

        for(var i = 0; i < enabledLangs.length; i++) {
            var opt = document.createElement('option');
            opt.value = enabledLangs[i];
            opt.innerHTML = enabledLangs[i].toUpperCase();

            if(enabledLangs[i] == instance.currentLang) {
                opt.selected = true;
            }

            instance.picker.appendChild(opt);
        }


        instance.picker.addEventListener('change', instance.fnOnPickerChange, false);
    },

    fnOnPickerChange: function(e) {
        instance.currentLang = this.options[this.selectedIndex].value;

        if(instance.settings.settings.cacheLanguage) {
            window.sessionStorage.setItem('lm__clang', instance.currentLang);
        }

        console.warn("[LanguageManager] Lang changed, current language detected: " + instance.currentLang + ".");
        instance.prepareLabels();
    },

    prepareLabels: function(){
        var dKey = '',
            currentText = '',
            currentDictionaryLevel = instance.isResponsive ? instance.currentScreenWidthRatio : 'default',
            currentEl;
        for(var i = 0; i < instance.elementsToDictionarize.length; i++) {
            currentEl = instance.elementsToDictionarize[i];
            dKey = currentEl.dataset.dkey;
            currentText = instance.settings.dictionary[dKey] ? instance.settings.dictionary[dKey][currentDictionaryLevel] : null;
            if(!currentText && instance.settings.dictionary[dKey]["default"]) {
                currentText = instance.settings.dictionary[dKey]["default"] || null;
                currentEl.innerHTML = currentText[instance.currentLang] || currentText[instance.setting.setting.defaultLang];
            } else if(currentText) {
                currentEl.innerHTML = currentText[instance.currentLang] || currentText[instance.setting.setting.defaultLang];
            } else {
                console.warn("[LanguageManager] No language item founded for: [" + JSON.stringify(instance.elementsToDictionarize[i]) + "][" + dKey + "][" + instance.currentLang + "]");
            }
        }
    },

    prepareForResponsive: function(){
        var timeout;
        var large = +instance.settings.settings.manageResponsive.large;
        var medium = +instance.settings.settings.manageResponsive.medium;
        var waitFor = +instance.settings.settings.waitBeforeRefreshScreenSize || 250;
        window.onresize = function(event) {
            clearTimeout(timeout);
            timeout = setTimeout(function(){
                var _currentWidth = window.innerWidth;
                if(_currentWidth >= large) {
                    instance.currentScreenWidthRatio = 'large';
                } else if (large > _currentWidth && _currentWidth >= medium) {
                    instance.currentScreenWidthRatio = 'medium';
                } else {
                    instance.currentScreenWidthRatio = 'small';
                }
                instance.prepareLabels();
                console.warn("[LanguageManager] Detected new screen size: [" + instance.currentScreenWidthRatio + "]");
            }, waitFor)
        };

        var currentWidth = window.innerWidth;
        
        if(currentWidth >= large) {
            instance.currentScreenWidthRatio = 'large';
        } else if (large > currentWidth && currentWidth >= medium) {
            instance.currentScreenWidthRatio = 'medium';
        } else {
            instance.currentScreenWidthRatio = 'small';
        }

        instance.prepareLabels();
    },

    loadJSON: function(pathToJson, cb) {   
            var xobj = new XMLHttpRequest();
                xobj.overrideMimeType("application/json");
            xobj.open('GET', pathToJson, true); // Replace 'my_data' with the path to your file
            xobj.onreadystatechange = function () {
                  if (xobj.readyState == 4 && xobj.status == "200") {
                    // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
                    cb(xobj.responseText);
                  }
            };
            xobj.send(null);  
         }
}